<?php

require_once("bitcoinController/bitcoin.inc.php");

// http or https
define('RPC_TYPE', variable_get('emc_rpc_type', "http"));
// as specified in your emercoin.conf configuration file
define('RPC_U_NAME', variable_get('emc_rpc_u_name', "emccoinrpc")); 
define('RPC_PASS', variable_get('emc_rpc_pass', ""));
define('RPC_HOST', variable_get('emc_rpc_host', "127.0.0.1")); // default port for emercoin
define('RPC_PORT', variable_get('emc_rpc_port', "8775"));
define('INFOCARD_CACHE_PATH', variable_get('emc_infocard_cache_path', '/var/tmp/infocard/'));
 
/**
 * Returns a singleton instance of the BitcoinClient class.
 */
function getBitcoinClient() {
static $client;
//public function __construct($scheme, $username, $password, 
//$address = "localhost", $port = 8332, 
//$certificate_path = '', $debug_level = 0) 
$client = new BitcoinClient(RPC_TYPE, RPC_U_NAME, RPC_PASS, RPC_HOST, RPC_PORT);
return $client;
}

/**
 * Validates emcSSL certificate from user's browser against blockchain.
 */
function emcssl_validate() {
$client = getBitcoinClient();
if (!$client->can_connect()) {
  return "Cannot connect to emercoin server";
}

    if(!array_key_exists('SSL_CLIENT_CERT', $_SERVER))
      return "No certificate presented, or missing flag +ExportCertData";

    if(!array_key_exists('SSL_CLIENT_I_DN_UID', $_SERVER))
      return "This certificane does not belong to a cryptocurrency";

    if($_SERVER['SSL_CLIENT_I_DN_UID'] != 'EMC')
      return "Wrong blockchain currency - this is not an EmerCoin blockchain certificate";

    // Generate search key, and retrieve NVS-value 
    $key = str_pad(strtolower($_SERVER['SSL_CLIENT_M_SERIAL']), 16, 0, STR_PAD_LEFT);
    if($key[0] == '0') 
      return "Wrong serial number, cannot start from zero";
    $key = "ssl:" . $key;
    $nvs = $client->query('name_show', $key);

    if($nvs['expires_in'] <= 0)
      return "NVS record has expired and is not trustable";

    // Compute certificate fingerprint, using algo, defined in the NVS value
    list($algo, $emc_fp) = explode('=', $nvs['value']);
    $crt_fp = hash($algo, 
              base64_decode(
              preg_replace('/\-+BEGIN CERTIFICATE\-+|-+END CERTIFICATE\-+|\n|\r/',
                       '', $_SERVER['SSL_CLIENT_CERT'])));

  $return_array = array();
  $return_array['key'] = $nvs['address'];
  if ($emc_fp == $crt_fp) {
    $return_array['auth'] = TRUE;
  }
  else {
    $return_array['auth'] = FALSE;
  }
  return $return_array;
} // emcssl_validate

/**
 * Runs two bowls.
 */
function emcssl_get_infocard() {
$emc_infocard = array();
$card_depth = 20;
$ic_ref = ($_SERVER['SSL_CLIENT_S_DN_UID']);
emcssl_infocard($ic_ref, $emc_infocard, $card_depth);
return $emc_infocard;

}

/**
 * bowl one.
 * Populate global array $emc_infocard
 * Before InfoCard usage:
 * mkdir /var/tmp/infocard/
 * chown www-data /var/tmp/infocard/
 */
function emcssl_infocard($ic_ref, &$emc_infocard, &$card_depth) {
  
  // global $emc_infocard, $card_depth;
  
  $client = getBitcoinClient();
  
  // echo "Called emcssl_infocard($ic_ref)\n";
  if(--$card_depth < 0) {
    return "Too long InfoCard reference chain";
  }
  // Remove possible hazardous symbols, for preserve shell injection
  list($service, $key, $passwd) = 
    explode(':', preg_replace('/[^0-9A-Za-z_:]/', '', $ic_ref));

  if($service != "info")
    return "Unsupported InfoCard service type: $service\n";

  if(!isset($passwd))
    return "Wrong InfoCard link format - missing password";

  $cached_path = INFOCARD_CACHE_PATH . $key;

  // If cached file too old (10+min) or non exist - read from NVS and create it
  if(!file_exists($cached_path) || time() - filemtime($cached_path) > 600) {
    try {
      $nvs = $client->query('name_show', "info:$key");
       // print_r($nvs);
       if($nvs['expires_in'] <= 0) {
         touch($cached_path);
         return "NVS record expired, and is not trustable";
       }
       $fh = popen("openssl aes-256-cbc -d -pass pass:$passwd | zcat > $cached_path", "wb");
       fwrite($fh, $nvs['value']);
       pclose($fh);
    } catch(Exception $e) {
      touch($cached_path);
      return "Unable fetch from NVS value for key=info:$key";
    } 
  }

  $fh = fopen($cached_path, "r");
  // Read InfoCard file, line by line
  $k     = "";
  $old_k = "";
  $loc_arr = array();
  $tpr = '_hash_' . getmypid() . '_';

  while(($buffer = fgets($fh, 4096)) !== false) {
    #echo "Buf=$buffer";
    preg_match('/^(\S+)?(\s+)(.+)?/', $buffer, $matches);
    // print_r($matches);
    if(isset($matches[1]) && !empty($matches[1]))
      $k = $matches[1];
    $v = "";
    if(isset($matches[3])) {
      $v = preg_replace('/\\\#/', $tpr, $matches[3]);
      $v = preg_replace('/\s*\#.*/', '', $v);
      $v = preg_replace("/$tpr/", '#', $v);
    }
    if(!empty($k) && !empty($v)) {
      if($k != $old_k) {
        // merge loc_arr to  $emc_infocard
        emcssl__merge($old_k, $loc_arr, $emc_infocard);
        $loc_arr = array();
      }
      array_push($loc_arr, $v);
      $old_k = $k;
    }
  } // while
  fclose($fh);

  // merge last array, if exist
  emcssl__merge($k, $loc_arr, $emc_infocard);
  return $emc_infocard;
} // emcssl_infocard


/**
 * bowl two.
 * Recursively calls spaghetti ball one.
 */
function emcssl__merge($k, $loc_arr, &$emc_infocard) {
  
  if(empty($k))
    return;
  //echo "Called merge for [$k]\n";
  //print_r($loc_arr);
  if($k == 'Import') {
  foreach ($loc_arr as $ic_ref)
    emcssl_infocard($ic_ref,  $emc_infocard);
  } else {
    preg_match('/([+]?)([^+]+)([+]?)/', $k, $matches);
    if(!isset($matches[2]) || empty($matches[2]))
      return; // Garbage key

    $q1 = isset($matches[1]) && !empty($matches[1]);
    $q3 = isset($matches[3]) && !empty($matches[3]);
    $k  = $matches[2];
 
    if(!$q1 && !$q3) {
      // key
      $emc_infocard[$k] = $loc_arr;
    }
    if($q1 && !$q3) {
      // +key
      $emc_infocard[$k] = isset($emc_infocard[$k])? 
        array_merge($emc_infocard[$k], $loc_arr) : $loc_arr;
    }
    if(!$q1 && $q3) {
      // key+
      $emc_infocard[$k] = isset($emc_infocard[$k])?
        array_merge($loc_arr, $emc_infocard[$k]) : $loc_arr;
    }
  }
  return $emc_infocard;
}
